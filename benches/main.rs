fn bench_throughput(c: &mut criterion::Criterion) {
	let mut group = c.benchmark_group("rand");

	for i in 0..12 {
		let kib = 2u64.pow(i);
		let size = 1024 * kib;
		let mut rng: rand_pcg::Pcg64Mcg = rand::SeedableRng::seed_from_u64(size);
		let data = rand::Rng::sample_iter(&mut rng, rand::distributions::Standard)
			.take(size.try_into().unwrap())
			.collect::<Vec<_>>();
		let expected = reservoir_capacity::resovoir_capacity_seq(&data);

		group.throughput(criterion::Throughput::Elements(size));
		let name = format!("{kib:>4} kiB");
		group.bench_with_input(
			criterion::BenchmarkId::new("seq", &name),
			&data,
			|b, data| {
				b.iter(|| {
					assert_eq!(reservoir_capacity::resovoir_capacity_seq(&data), expected);
				});
			});
		group.bench_with_input(
			criterion::BenchmarkId::new("parallel", name),
			&data,
			|b, data| {
				b.iter(|| {
					assert_eq!(reservoir_capacity::resovoir_capacity_par(&data), expected);
				});
			});
	}

	group.finish();
}

criterion::criterion_group!(benches,
	bench_throughput,
);
criterion::criterion_main!(benches);
