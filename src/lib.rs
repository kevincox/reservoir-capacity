type NextPtr<T> = Option<std::ptr::NonNull<Link<T>>>;

#[derive(Clone,Debug)]
struct Link<T> {
	next: NextPtr<T>,
	value: T,
}

#[derive(Clone,Debug)]
struct ListHead<T> {
	head: std::ptr::NonNull<Link<T>>,
	tail: std::ptr::NonNull<NextPtr<T>>,
}

#[derive(Clone,Debug)]
struct List<T>(Option<ListHead<T>>);

unsafe impl<T: Send> Send for List<T> {}

impl<T> Drop for List<T> {
	fn drop(&mut self) {
		while let Some(_) = self.pop() {}
	}
}

impl<T> List<T> {
	fn push(&mut self, value: T) {
		if let Some(ListHead { ref mut head, tail: _ }) = &mut self.0 {
			*head = Box::leak(Box::new(Link{
				next: Some(*head),
				value,
			})).into();
		} else {
			let link = Box::leak(Box::new(Link{
				next: None,
				value,
			}));
			self.0 = Some(ListHead {
				head: link.into(),
				tail: (&mut link.next).into(),
			})
		}
	}

	fn pop(&mut self) -> Option<T> {
		let ListHead { head, tail: _ } = self.0.as_mut()?;
		let Link { next, value } = *unsafe { Box::from_raw(head.as_mut()) };
		if let Some(next) = next {
			*head = next.into();
		} else {
			self.0 = None;
		}
		Some(value)
	}

	fn last_mut(&mut self) -> Option<&mut T> {
		self.0.as_mut().map(|l| unsafe { &mut l.head.as_mut().value })
	}

	fn extend(&mut self, mut that: List<T>) {
		let Some(ListHead { head: ref mut this_head, tail: _ }) = self.0 else {
			*self = that;
			return
		};
		let Some(ListHead { head: that_head, tail: mut that_tail }) = that.0.take() else { return };

		unsafe {
			debug_assert!(that_tail.as_mut().is_none());
			*that_tail.as_mut() = Some(*this_head)
		};
		*this_head = that_head;
	}
}

impl<T> Default for List<T> {
	fn default() -> Self {
		return List(None)
	}
}

impl<T> Iterator for List<T> {
	type Item = T;
	fn next(&mut self) -> Option<Self::Item> {
		self.pop()
	}
}

#[test]
fn list() {
	let list = List::<()>::default();
	assert_eq!(list.collect::<Vec<_>>(), &[]);

	let mut list = List::default();
	list.push(1);
	list.push(2);
	list.push(3);
	assert_eq!(list.collect::<Vec<_>>(), &[3, 2, 1]);

	let mut list = List::default();
	list.push(1);
	assert_eq!(list.pop(), Some(1));
	list.push(2);
	list.push(3);
	assert_eq!(list.collect::<Vec<_>>(), &[3, 2]);

	let mut l1 = List::default();
	l1.push(1);
	l1.push(2);
	l1.extend(List::default());
	assert_eq!(l1.collect::<Vec<_>>(), &[2, 1]);

	let mut l1 = List::default();
	let mut l2 = List::default();
	l2.push(1);
	l2.push(2);
	l1.extend(l2);
	assert_eq!(l1.collect::<Vec<_>>(), &[2, 1]);

	let mut l1 = List::default();
	l1.push(1);
	l1.push(2);
	let mut l2 = List::default();
	l2.push(3);
	l2.push(4);
	l1.extend(l2);
	assert_eq!(l1.collect::<Vec<_>>(), &[4, 3, 2, 1]);
}

#[derive(Clone,Debug)]
struct Segment {
	width: u32,
	height: u8,
}

impl Segment {
	fn fill(&self, mut left: Segment, mut right: Segment) -> (Segment, Segment, u64) {
		debug_assert!(left.height > self.height);
		debug_assert!(right.height > self.height);

		let capacity;
		if left.height > right.height {
			right.width += self.width;
			capacity = u64::from(right.height - self.height) * u64::from(self.width);
		} else {
			left.width += self.width;
			capacity = u64::from(left.height - self.height) * u64::from(self.width);
		}

		(left, right, capacity)
	}
}

pub fn resovoir_capacity_seq(levels: &[u8]) -> u64 {
	let mut levels = levels.iter().copied();

	let Some(height) = levels.next() else { return 0 };
	let mut middle = Segment{
		width: 0,
		height,
	};
	let mut left = Vec::<Segment>::new();
	let mut r = 0;

	for height in levels {
		let mut right = Segment{ height, width: 1 };
		while right.height > middle.height {
			if let Some(l) = left.pop() {
				let merge = middle.fill(l, right);
				middle = merge.0;
				right = merge.1;
				r += merge.2;
			} else {
				middle = Segment{height: right.height, width: 0};
				break;
			}
		}

		if right.height < middle.height {
			left.push(middle);
			middle = right;
		} else {
			debug_assert_eq!(right.height, middle.height);
			middle.width += 1;
		}
	}

	return r;
}

#[derive(Clone,Debug)]
struct ParState {
	left: List<Segment>,
	peak: Segment,
	right: List<Segment>,
	capacity: u64,
}

impl ParState {
	fn merge(mut self, mut right: ParState) -> ParState {
		let fill = if self.peak.height > right.peak.height {
			&mut right.peak
		} else {
			&mut self.peak
		};

		self.capacity += right.capacity;
		self.capacity += fill_to(&mut self.right, fill);
		self.capacity += fill_to(&mut right.left, fill);

		if self.peak.height < right.peak.height {
			right.left.push(self.peak);

			self.peak = right.peak;
		} else {
			self.right.push(right.peak);
		}

		right.left.extend(self.left);
		self.left = right.left;
		self.right.extend(right.right);

		self
	}
}

fn fill_to(slope: &mut List<Segment>, level: &mut Segment) -> u64 {
	let mut capacity = 0;
	loop {
		let Some(segment) = slope.last_mut() else { break };
		match segment.height.cmp(&level.height) {
			std::cmp::Ordering::Greater => {
				break;
			}
			std::cmp::Ordering::Less => {
				capacity += u64::from(level.height - segment.height) * u64::from(segment.width);
				level.width += segment.width;
			}
			std::cmp::Ordering::Equal => {
				level.width += segment.width;
			}
		}
		slope.pop();
	}
	capacity
}

pub fn resovoir_capacity_par(levels: &[u8]) -> u64 {
	resovoir_capacity_inc(levels).capacity
}

fn resovoir_capacity_inc(levels: &[u8]) -> ParState {
	match levels.len() {
		0 => ParState {
			left: Default::default(),
			peak: Segment{height: 0, width: 0},
			right: Default::default(),
			capacity: 0,
		},
		1 => ParState {
			left: Default::default(),
			peak: Segment{height: levels[0], width: 1},
			right: Default::default(),
			capacity: 0,
		},
		len => {
			let (l, r) = levels.split_at(len / 2);
			// let (l, r) = (resovoir_capacity_inc(l), resovoir_capacity_inc(r));
			let (l, r) = rayon::join(
				|| resovoir_capacity_inc(l),
				|| resovoir_capacity_inc(r));
			// dbg!(l.clone(), r.clone(), l.clone().merge(r.clone()));
			l.merge(r)
		}
	}
}

#[test]
fn trivial() {
	assert_eq!(resovoir_capacity_seq(&[]), 0);
	assert_eq!(resovoir_capacity_par(&[]), 0);

	assert_eq!(resovoir_capacity_seq(&[0]), 0);
	assert_eq!(resovoir_capacity_par(&[0]), 0);

	assert_eq!(resovoir_capacity_seq(&[1]), 0);
	assert_eq!(resovoir_capacity_par(&[1]), 0);

	assert_eq!(resovoir_capacity_seq(&[u8::MAX]), 0);
	assert_eq!(resovoir_capacity_par(&[u8::MAX]), 0);
}

#[test]
fn peak() {
	let data = &[1, 4, 2];
	assert_eq!(resovoir_capacity_seq(data), 0);
	assert_eq!(resovoir_capacity_par(data), 0);

	let data = &[8, 16, 16, 20, 14, 2];
	assert_eq!(resovoir_capacity_seq(data), 0);
	assert_eq!(resovoir_capacity_par(data), 0);
}

#[test]
fn valley_1() {
	let data = &[8, 4, 12];
	assert_eq!(resovoir_capacity_seq(data), 4);
	assert_eq!(resovoir_capacity_par(data), 4);
}

#[test]
fn valley_2() {
	let data = &[10, 8, 4, 9];
	assert_eq!(resovoir_capacity_seq(data), 6);
	assert_eq!(resovoir_capacity_par(data), 6);
}

#[test]
fn valley_many() {
	let data = &[3, 8, 10, 9, 2, 4, 2, 8, 12, 20, 10, 4, 5];
	assert_eq!(resovoir_capacity_seq(data), 26);
	assert_eq!(resovoir_capacity_par(data), 26);
}

#[test]
fn test_rand() {
	let data = [62, 204, 126, 195, 126, 137];
	assert_eq!(resovoir_capacity_seq(&data), 80);
	assert_eq!(resovoir_capacity_par(&data), 80);
}
